#This file consists of the basic definitions needed to create the chat-system

namespace cpp start

map< i16 , string > clientID

service Chat {

	oneway void send_message( 1:i16 receptorID, 2:string message ) ,

	pair< i16 , string > receive_message( )

}
