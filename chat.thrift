namespace cpp chat
namespace java chat
namespace php chat
namespace perl chat

struct message{
	1: string clientName, 
	2: string reciever,
	3: string messageContent,
}

service Chat {
	void ping(),
	bool connect(1: string clientName),
	void sendMessage(1:message M),	
	list<message> fetchMessages(1:string clientName),

}
